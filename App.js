/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import {
  View, FlatList, ScrollView, Text,StyleSheet, TouchableOpacity
} from 'react-native';

import FetchUserList from './src/components/FetchUserList';
import Header from './src/components/Header';
import DisplayUserList from './src/components/DisplayUserList';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import uuid from 'react-native-uuid';

const App = () => {


  const [userList, setUserList] = useState([]);


  // delete item method
  const fillUserList = (list) => { //setting the filtered list back into the state
    console.log("fillUserList() method called " + list)
    setUserList(list);
    console.log(list)
  }




  return (
    <View style={styles.containerView}>
      {/* component for Header purpose */}
      <Header />
      {/* component to make axios call api, to fetch userList and update the App 'userList' state  */}
      <FetchUserList fillUserListMethod={fillUserList} />
      {/* component to display the 'userList' inside a TouchableOpacity Component 
     <DisplayUserList item="SS"/>
     */}

      {/* //uncommit this to see all data in FlatList 
     <FlatList  data={userList} renderItem={({item})=><DisplayUserList item={item}/>} />
    */}


      <ScrollView>
        {
          userList.map((user, index) =>
          <TouchableOpacity key={uuid.v1()}> 
            <View style={styles.userViewStyle}>
              <Text> ID :{user.id} </Text>
              <Text> Name :{user.name} </Text>
              <Text> UserName :{user.username} </Text>
              <Text> Email :{user.email} </Text>
              <Text> Phone :{user.phone} </Text>
              <Text> Website :{user.website} </Text>           
              <Icon name="remove" size={30} color="firebrick" 
              onPress={()=>alert("Do you want to Delete this item")} />

            </View>
            </TouchableOpacity>
          )

        }
      </ScrollView>



    </View>
  )


}

const styles=StyleSheet.create({

containerView:{
  margin: 2,
  flex:1
},
userViewStyle:{
  
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
    margin: 2,
    borderColor: '#2a4944',
    borderWidth: 1,
    backgroundColor: '#d2f7f1'
}



});

export default App;
