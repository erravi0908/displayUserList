# displayUserList

# A Simple App Which Displays User List by Fetching them From  a baseURL:  https://jsonplaceholder.typicode.com/users

# Programming Concepts Used in this App

# 1. Promise based Axios API with Get() Functionality.
# 2. Concept of useEffect() hook, For making a axios.get(baseURL) request 
# 3. A Child Component is used to make this axios.get(baseURL) inside which the useEffect() is used.
# 4. async function is used inside useEffect() hook, 
# 5. await axios.get(baseURL) concept is used which makes axios return an Response object instead of a Promise.
# 6. Child Component updated the Parent Container state, once Child Container fetched the response from the axios.get(baseURl)


# purpose of each React Native Component File (.js) File used
# 1.  App.js ( acts as parent Container for the entire App)
#            it contains state variable named 'userList'   
#               const [userList, setUserList] = useState([]); 

#  Other Components available in src/components folder
#  1. Header.js :- To Display the Header as "User List"

#  2. FetchUserList.js :- Its Child Component, it uses useEffect() hook to make async await axios.get(baseUrl) call to fetch the 
#                         the user Data and then pass this Data to App.js state variable i.e userList

#  3. DisplayUserList.js : It acts as view Component, and uses ScrollView to show the userList


# Dependencies used in App as defined in "dependencies" section of package.json

# "dependencies": {
#    "axios": "^0.21.0",
#    "buffer": "^6.0.3",
#    "react": "16.13.1",
#    "react-native": "0.63.3",
#    "react-native-uuid": "^1.4.9",
#    "react-native-vector-icons": "^7.1.0"
#  },
