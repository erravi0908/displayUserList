
import React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';

const DisplayUserList = ({item}) => {

    return (
        <TouchableOpacity>
            <View style={styles.listViewStyle}>
             <Text style={styles.listTextStyle}>
                    User Name: {item.username} 
            </Text>
            <Icon name="remove" size={20} color="firebrick" onPress={()=>deleteItem(item.id)}/> 
            </View>
        </TouchableOpacity>


    )


}

const styles = StyleSheet.create({

    listViewStyle: {
        marginTop: 10,
        height: 60,
        backgroundColor: 'lightgrey',
        alignItems: 'center',
        justifyContent: 'center'
        

    },
    listTextStyle:
    {
        fontSize: 20,
        color: 'black'
    }

})


export default DisplayUserList;