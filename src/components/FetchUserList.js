
import React, { useEffect } from 'react'
import {
    View,
    Text
} from 'react-native';
import axios from 'axios';


const baseUrl = "https://jsonplaceholder.typicode.com/users";




const FetchUserList = ({ fillUserListMethod }) => {

   

    useEffect(() => {

        var result={};
        // What is an async function: They return promise instead of values. the values
        // will be input arguments of the promise returned by them.

        // async function , it output will be an promise(argument) object method
        //  and the argument will contain whatever is mentioed in the return of this async function 
        async function getData() {

            // Syntax1:
            // This will also work , as axios.get() returns a promise 
            // but we prefer the 2nd Syntax, becuase by using await keyword the promise based function call of axios 
            // returns the value as ouput , instead of promise. which we dont want.
            //const promise=  axios.get(baseUrl);

            // syntax 2: ; in it axios returns response object, using
            // its response.status property we can return data or error
            const response = await axios.get(baseUrl);

            console.log("Response Status" + response.status);
            console.log("Response Data " + response.data);
            console.log("Response Data length" + response.data.length);

            // if success response we give below response
            if (response.status == 200) {

                console.log("200 Status if Block executed")
                // fill the result object
                result={data:response.data, statusCode:response.status};
                // success response ; we return Data and status Code
                return result;

            } else {

                console.log("Else Status Block executed")
                // Any other  responsestatus ; Then we return Data and status Code

                // fill the result object, with empty data
                result={data:[], statusCode:response.status};

                return result;
            }

        }

        // calling the async getData() function
        var  promiseReturnedFromAsyncMethod=getData();
        // then we can use this promise to pass the return 'result'  data to
        // a method of other component , and that method can update the State of that component
        // right now we are just displaying it in the console.
        promiseReturnedFromAsyncMethod.then( resultReturnedFromAsyncMethod=>
            { 
                console.log("Displaying the resultReturnedFromAsyncMethod statusCode property value -->" +resultReturnedFromAsyncMethod.statusCode) 
                console.log("Displaying the resultReturnedFromAsyncMethod data value -->" +resultReturnedFromAsyncMethod.data) 
            
               
                //calling the fillUserListMethod() of App.js 
                fillUserListMethod(resultReturnedFromAsyncMethod.data);
            })

            
        
    }, []);



    return (
        <></>

    )



}


export default FetchUserList;