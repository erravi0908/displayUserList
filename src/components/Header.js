
import React from 'react';
import { Text, View,StyleSheet } from 'react-native';

const Header = () => {

    return (
        <View style={styles.headerViewStyle}>
            <Text style={styles.headerTextStyle}>
                    USER  LIST 
            </Text>

        </View>


    )


}

const styles=StyleSheet.create({

    headerViewStyle:{
        marginTop:10,
        height:60,
        backgroundColor:'green',
        alignItems:'center',
        justifyContent:'center'
        
    },
    headerTextStyle:
    {
        fontSize:20,
        color:'white'
    }

})


export default Header;